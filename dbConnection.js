var mongoose = require("mongoose");

mongoose.connect("mongodb://localhost/stock");

var stockSchema = new mongoose.Schema({
	name: String,
	price: Number,
	stockNumber: Number
});

var Product = mongoose.model("Product", stockSchema);

exports.addNewProduct = function (name, price, stockNumber){
	var product = new Product({
		name: name,
		price: price,
		stockNumber: stockNumber
	});
	product.save(function(err, product){
		if(err){
			console.log("Something is wrong with saving");
		} else {
			console.log("Saved");	
		}
	});
}

exports.checkStock = function (item) {
	 return new Promise((resolve, reject) => {
		Product.findOne({name: item.name}, function(err, product){
			if(err){
				console.log("Something is wrong with retrieving");
				reject();
			} else {
				console.log("Retrieved");
				//console.log(products);
				if(product && product.stockNumber > 0){
					resolve();
				} else {
					reject();				
				}
			}
		});
	})
}

process.on('SIGINT', function() {
	mongoose.connection.close(function () {
	  console.log('Mongoose disconnected on app termination');
	  process.exit(0);
	});
  });
