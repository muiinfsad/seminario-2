var cartFunctions = require("./CartFunctions.js");
var modulo2 = require("./dbConnection.js");

function Item(id, name, price) {
    this.id = id;
    this.name = name;
    this.price = price;
};



var cart = [];

var item1 = new Item(2, "Naranja", 2);
var item2 = new Item(3, "Pera", 2);
console.log("Adding item1....");
cartFunctions.addToCart(item1, cart);
console.log("Adding item2....");
cartFunctions.addToCart(item2, cart);
console.log("Removing item1....");
setTimeout(()=>{cartFunctions.removeFromCart(item1.id, cart)},2000);



