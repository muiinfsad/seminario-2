var modulo2 = require("./dbConnection.js");

exports.addToCart = function (item, cart) {
	modulo2.checkStock(item).then(() => {
		cart.push(item);
        console.log("Added. Cart: ");
        console.log(cart);
	}, () => {
        console.log("There is no stock available for this item");
    });
}

exports.removeFromCart = function (itemId, cart) {
    console
    var removedItem = null;
    for (var itemIndex = 0; itemIndex < cart.length; itemIndex++){
        if (cart[itemIndex].id == itemId)
            removedItem = cart.splice(itemIndex, 1);
    }
    console.log("Removed. Cart: ");
    console.log(cart);
   
}
