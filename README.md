# Seminario 2
 Para el correcto funcionamiento del proyecto será necesario ejecutar en la carpeta raíz el comando ```npm install ``` .
## Parte 1 
 En primer lugar se debe ejecutar el módulo chargeDB.js con el comando ``` node chargeDB.js ```. La ejecución de este comando cargará en la base de datos de mongoDB un item con stock 10 llamado Naranja. En segundo lugar se 
 ejecutará el fichero ejecutable.js con el siguiente comando ``` node ejecutable.js ```. Este ejecutable añadirá dos elementos al carrito: primero la Naranja previamente añadida a la base de datos, y después una Pera, que no existe en la 
 base de datos. Por último, se elimina la Naranja. La eliminación de la Naranja se lleva a cabo después de 2 segundos, usando ```setTimeout``` y una función callback, para que cuando se llame a la función que elimina la naranja esta se haya agregado
 ya al carrito. En la terminal se puede observar el estado del carrito después de cada acción, que pasa de estar vacío a contener una Naranja, y luego vuelve a estar vacío.
## Parte 2 
 La parte opcional se ha realizado en el archivo dbConnection.js, utilizado por el archivo CartFunctions, que es donde están las funciones de añadir y quitar elementos al carrito. En ese módulo existe una función llamada
 checkStock que devuelve una promesa la cual se encarga de buscar el elemento en la base de datos y ejecuta ```resolve()``` si el producto se encuentra en la base de datos y hay stock igual o superior al que se quiere añadir al carrito.
 Pero si el producto no se encuentra en la base de datos o no hay stock suficiente se rechaza con ```reject()```.
 

